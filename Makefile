include ${COSMOSIS_SRC_DIR}/config/compilers.mk

USER_FFLAGS=-I.
USER_LDFLAGS=-lcosmosis_fortran

export EQUATIONS ?= equations
export RECOMBINATION ?= recfast

INTERFACE=camb.so
CAMBLIB=libcamb.a
CAMBDIR=CAMB-1.0.3_ddm/fortran
CAMBLIBDIR=CAMB-1.0.3_ddm/fortran/Release
CAMBUTILDIR=CAMB-1.0.3_ddm/forutils/Release
CAMBUTIL = libforutils.a

all: $(INTERFACE)

# purposefully chosen to be non-file target to
# always decend into CAMBDIR
$(CAMBLIBDIR)/$(CAMBLIB): $(CAMBUTILDIR)/$(CAMBUTIL)
	cd $(CAMBDIR) && make libcamb

$(CAMBUTILDIR)/$(CAMBUTIL)::
	cd CAMB-1.0.3_ddm/forutils && make Release

$(INTERFACE): $(CAMBLIBDIR)/$(CAMBLIB) $(CAMBUTILDIR)/$(CAMBUTIL) camb_interface.f90 camb_module.f90
	$(FC) $(FFLAGS) -shared camb_interface.f90 camb_module.f90 -o $(INTERFACE) $(LDFLAGS) -I$(CAMBLIBDIR) -I$(CAMBUTILDIR) -L$(CAMBLIBDIR) -lcamb -L$(CAMBUTILDIR) -lforutils
clean:
	cd $(CAMBDIR) && $(MAKE) clean
	rm -rf *.so *.o *.mod *.dSYM

test:
	@echo "Alas, camb has no tests"
