subroutine gamma ( x, ga )

!*****************************************************************************80
!
!! GAMMA evaluates the Gamma function.
!
!  Licensing:
!
!    The original FORTRAN77 version of this routine is copyrighted by 
!    Shanjie Zhang and Jianming Jin.  However, they give permission to 
!    incorporate this routine into a user program that the copyright 
!    is acknowledged.
!
!  Modified:
!
!    08 September 2007
!
!  Author:
!
!    Original FORTRAN77 version by Shanjie Zhang, Jianming Jin.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Shanjie Zhang, Jianming Jin,
!    Computation of Special Functions,
!    Wiley, 1996,
!    ISBN: 0-471-11963-6,
!    LC: QA351.C45
!
!  Parameters:
!
!    Input, real ( kind = 8 ) X, the argument.
!    X must not be 0, or any negative integer.
!
!    Output, real ( kind = 8 ) GA, the value of the Gamma function.
!
  implicit none

  real ( kind = 8 ), dimension ( 26 ) :: g = (/ &
    1.0D+00, &
    0.5772156649015329D+00, &
   -0.6558780715202538D+00, &
   -0.420026350340952D-01, &
    0.1665386113822915D+00, &
   -0.421977345555443D-01, &
   -0.96219715278770D-02, &
    0.72189432466630D-02, &
   -0.11651675918591D-02, &
   -0.2152416741149D-03, &
    0.1280502823882D-03, & 
   -0.201348547807D-04, &
   -0.12504934821D-05, &
    0.11330272320D-05, &
   -0.2056338417D-06, & 
    0.61160950D-08, &
    0.50020075D-08, &
   -0.11812746D-08, &
    0.1043427D-09, & 
    0.77823D-11, &
   -0.36968D-11, &
    0.51D-12, &
   -0.206D-13, &
   -0.54D-14, &
    0.14D-14, &
    0.1D-15 /)
  real ( kind = 8 ) ga
  real ( kind = 8 ) gr
  integer ( kind = 4 ) k
  integer ( kind = 4 ) m
  integer ( kind = 4 ) m1
  real ( kind = 8 ), parameter :: pi = 3.141592653589793D+00
  real ( kind = 8 ) r
  real ( kind = 8 ) x
  real ( kind = 8 ) z

  if ( x == aint ( x ) ) then

    if ( 0.0D+00 < x ) then
      ga = 1.0D+00
      m1 = int ( x ) - 1
      do k = 2, m1
        ga = ga * k
      end do
    else
      ga = 1.0D+300
    end if

  else

    if ( 1.0D+00 < abs ( x ) ) then
      z = abs ( x )
      m = int ( z )
      r = 1.0D+00
      do k = 1, m
        r = r * ( z - real ( k, kind = 8 ) )
      end do
      z = z - real ( m, kind = 8 )
    else
      z = x
    end if

    gr = g(26)
    do k = 25, 1, -1
      gr = gr * z + g(k)
    end do

    ga = 1.0D+00 / ( gr * z )

    if ( 1.0D+00 < abs ( x ) ) then
      ga = ga * r
      if ( x < 0.0D+00 ) then
        ga = - pi / ( x* ga * sin ( pi * x ) )
      end if
    end if

  end if

  return
end
subroutine gmn ( m, n, c, x, bk, gf, gd )

!*****************************************************************************80
!
!! GMN computes quantities for oblate radial functions with small argument.
!
!  Discussion:
!
!    This procedure computes Gmn(-ic,ix) and its derivative for oblate
!    radial functions with a small argument.
!
!  Licensing:
!
!    This routine is copyrighted by Shanjie Zhang and Jianming Jin.  However, 
!    they give permission to incorporate this routine into a user program 
!    provided that the copyright is acknowledged.
!
!  Modified:
!
!    15 July 2012
!
!  Author:
!
!    Shanjie Zhang, Jianming Jin
!
!  Reference:
!
!    Shanjie Zhang, Jianming Jin,
!    Computation of Special Functions,
!    Wiley, 1996,
!    ISBN: 0-471-11963-6,
!    LC: QA351.C45.
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) M, the mode parameter;  M = 0, 1, 2, ...
!
!    Input, integer ( kind = 4 ) N, mode parameter, N = M, M + 1, M + 2, ...
!
!    Input, real ( kind = 8 ) C, spheroidal parameter.
!
!    Input, real ( kind = 8 ) X, the argument.
!
!    Input, real ( kind = 8 ) BK(*), coefficients.
!
!    Output, real ( kind = 8 ) GF, GD, the value of Gmn(-C,X) and Gmn'(-C,X).
!
  implicit none

  real ( kind = 8 ) bk(200)
  real ( kind = 8 ) c
  real ( kind = 8 ) eps
  real ( kind = 8 ) gd
  real ( kind = 8 ) gd0
  real ( kind = 8 ) gd1
  real ( kind = 8 ) gf
  real ( kind = 8 ) gf0
  real ( kind = 8 ) gw
  integer ( kind = 4 ) ip
  integer ( kind = 4 ) k
  integer ( kind = 4 ) m
  integer ( kind = 4 ) n
  integer ( kind = 4 ) nm
  real ( kind = 8 ) x
  real ( kind = 8 ) xm

  eps = 1.0D-14

  if ( n - m == 2 * int ( ( n - m ) / 2 ) ) then
    ip = 0
  else
    ip = 1
  end if

  nm = 25 + int ( 0.5D+00 * ( n - m ) + c )
  xm = ( 1.0D+00 + x * x ) ** ( -0.5D+00 * m )
  gf0 = 0.0D+00
  do k = 1, nm
    gf0 = gf0 + bk(k) * x ** ( 2.0D+00 * k - 2.0D+00 )
    if ( abs ( ( gf0 - gw ) / gf0 ) < eps .and. 10 <= k ) then
      exit
    end if
    gw = gf0
  end do

  gf = xm * gf0 * x ** ( 1 - ip )

  gd1 = - m * x / ( 1.0D+00 + x * x ) * gf
  gd0 = 0.0D+00

  do k = 1, nm

    if ( ip == 0 ) then
      gd0 = gd0 + ( 2.0D+00 * k - 1.0D+00 ) * bk(k) &
        * x ** ( 2.0D+00 * k - 2.0D+00 )
    else
      gd0 = gd0 + 2.0D+00 * k * bk(k+1) * x ** ( 2.0D+00 * k - 1.0D+00 )
    end if

    if ( abs ( ( gd0 - gw ) / gd0 ) < eps .and. 10 <= k ) then
      exit
    end if

    gw = gd0

  end do

  gd = gd1 + xm * gd0

  return
end
subroutine herzo ( n, x, w )

!*****************************************************************************80
!
!! HERZO computes the zeros the Hermite polynomial Hn(x).
!
!  Discussion:
!
!    This procedure computes the zeros of Hermite polynomial Ln(x)
!    in the interval [-1,+1], and the corresponding
!    weighting coefficients for Gauss-Hermite integration.
!
!  Licensing:
!
!    This routine is copyrighted by Shanjie Zhang and Jianming Jin.  However, 
!    they give permission to incorporate this routine into a user program 
!    provided that the copyright is acknowledged.
!
!  Modified:
!
!    15 July 2012
!
!  Author:
!
!    Shanjie Zhang, Jianming Jin
!
!  Reference:
!
!    Shanjie Zhang, Jianming Jin,
!    Computation of Special Functions,
!    Wiley, 1996,
!    ISBN: 0-471-11963-6,
!    LC: QA351.C45.
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) N, the order of the polynomial.
!
!    Output, real ( kind = 8 ) X(N), the zeros.
!
!    Output, real ( kind = 8 ) W(N), the corresponding weights.
!
  implicit none

  integer ( kind = 4 ) n

  real ( kind = 8 ) f0
  real ( kind = 8 ) f1
  real ( kind = 8 ) fd
  real ( kind = 8 ) gd
  real ( kind = 8 ) hd
  real ( kind = 8 ) hf
  real ( kind = 8 ) hn
  integer ( kind = 4 ) i
  integer ( kind = 4 ) it
  integer ( kind = 4 ) j
  integer ( kind = 4 ) k
  integer ( kind = 4 ) nr
  real ( kind = 8 ) p
  real ( kind = 8 ) q
  real ( kind = 8 ) r
  real ( kind = 8 ) r1
  real ( kind = 8 ) r2
  real ( kind = 8 ) w(n)
  real ( kind = 8 ) wp
  real ( kind = 8 ) x(n)
  real ( kind = 8 ) x0
  real ( kind = 8 ) z
  real ( kind = 8 ) z0
  real ( kind = 8 ) zl

  hn = 1.0D+00 / n
  zl = -1.1611D+00 + 1.46D+00 * sqrt ( real ( n, kind = 8 ) )

  do nr = 1, n / 2

    if ( nr == 1 ) then
      z = zl
    else
      z = z - hn * ( n / 2 + 1 - nr )
    end if

    it = 0

    do

      it = it + 1
      z0 = z
      f0 = 1.0D+00
      f1 = 2.0D+00 * z
      do k = 2, n
        hf = 2.0D+00 * z * f1 - 2.0D+00 * ( k - 1.0D+00 ) * f0
        hd = 2.0D+00 * k * f1
        f0 = f1
        f1 = hf
      end do

      p = 1.0D+00
      do i = 1, nr - 1
        p = p * ( z - x(i) )
      end do
      fd = hf / p

      q = 0.0D+00
      do i = 1, nr - 1
        wp = 1.0D+00
        do j = 1, nr - 1
          if ( j /= i ) then
            wp = wp * ( z - x(j) )
          end if
        end do
        q = q + wp
      end do

      gd = ( hd - q * fd ) / p
      z = z - fd / gd

      if ( 40 < it .or. abs ( ( z - z0 ) / z ) <= 1.0D-15 ) then
        exit
      end if

    end do

    x(nr) = z
    x(n+1-nr) = -z
    r = 1.0D+00
    do k = 1, n
      r = 2.0D+00 * r * k
    end do
    w(nr) = 3.544907701811D+00 * r / ( hd * hd )
    w(n+1-nr) = w(nr)

  end do

  if ( n /= 2 * int ( n / 2 ) ) then
    r1 = 1.0D+00
    r2 = 1.0D+00
    do j = 1, n
      r1 = 2.0D+00 * r1 * j
      if ( ( n + 1 ) / 2 <= j ) then
        r2 = r2 * j
      end if
    end do
    w(n/2+1) = 0.88622692545276D+00 * r1 / ( r2 * r2 )
    x(n/2+1) = 0.0D+00
  end if

  return
end
subroutine hygfx ( a, b, c, x, hf )

!*****************************************************************************80
!
!! HYGFX evaluates the hypergeometric function F(A,B,C,X).
!
!  Licensing:
!
!    The original FORTRAN77 version of this routine is copyrighted by 
!    Shanjie Zhang and Jianming Jin.  However, they give permission to 
!    incorporate this routine into a user program that the copyright 
!    is acknowledged.
!
!  Modified:
!
!    08 September 2007
!
!  Author:
!
!    Original FORTRAN77 version by Shanjie Zhang, Jianming Jin.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Shanjie Zhang, Jianming Jin,
!    Computation of Special Functions,
!    Wiley, 1996,
!    ISBN: 0-471-11963-6,
!    LC: QA351.C45
!
!  Parameters:
!
!    Input, real ( kind = 8 ) A, B, C, X, the arguments of the function.
!    C must not be equal to a nonpositive integer.
!    X < 1.
!
!    Output, real HF, the value of the function.
!
  implicit none

  real ( kind = 8 ) a
  real ( kind = 8 ) a0
  real ( kind = 8 ) aa
  real ( kind = 8 ) b
  real ( kind = 8 ) bb
  real ( kind = 8 ) c
  real ( kind = 8 ) c0
  real ( kind = 8 ) c1
  real ( kind = 8 ), parameter :: el = 0.5772156649015329D+00
  real ( kind = 8 ) eps
  real ( kind = 8 ) f0
  real ( kind = 8 ) f1
  real ( kind = 8 ) g0
  real ( kind = 8 ) g1
  real ( kind = 8 ) g2
  real ( kind = 8 ) g3
  real ( kind = 8 ) ga
  real ( kind = 8 ) gabc
  real ( kind = 8 ) gam
  real ( kind = 8 ) gb
  real ( kind = 8 ) gbm
  real ( kind = 8 ) gc
  real ( kind = 8 ) gca
  real ( kind = 8 ) gcab
  real ( kind = 8 ) gcb
  real ( kind = 8 ) gm
  real ( kind = 8 ) hf
  real ( kind = 8 ) hw
  integer ( kind = 4 ) j
  integer ( kind = 4 ) k
  logical l0
  logical l1
  logical l2
  logical l3
  logical l4
  logical l5
  integer ( kind = 4 ) m
  integer ( kind = 4 ) nm
  real ( kind = 8 ) pa
  real ( kind = 8 ) pb
  real ( kind = 8 ), parameter :: pi = 3.141592653589793D+00
  real ( kind = 8 ) r
  real ( kind = 8 ) r0
  real ( kind = 8 ) r1
  real ( kind = 8 ) rm
  real ( kind = 8 ) rp
  real ( kind = 8 ) sm
  real ( kind = 8 ) sp
  real ( kind = 8 ) sp0
  real ( kind = 8 ) x
  real ( kind = 8 ) x1

  l0 = ( c == aint ( c ) ) .and. ( c < 0.0D+00 )
  l1 = ( 1.0D+00 - x < 1.0D-15 ) .and. ( c - a - b <= 0.0D+00 )
  l2 = ( a == aint ( a ) ) .and. ( a < 0.0D+00 )
  l3 = ( b == aint ( b ) ) .and. ( b < 0.0D+00 )
  l4 = ( c - a == aint ( c - a ) ) .and. ( c - a <= 0.0D+00 )
  l5 = ( c - b == aint ( c - b ) ) .and. ( c - b <= 0.0D+00 )

  if ( l0 .or. l1 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'HYGFX - Fatal error!'
    write ( *, '(a)' ) '  The hypergeometric series is divergent.'
    return
  end if

  if ( 0.95D+00 < x ) then 
    eps = 1.0D-08
  else
    eps = 1.0D-15
  end if

  if ( x == 0.0D+00 .or. a == 0.0D+00 .or. b == 0.0D+00 ) then

    hf = 1.0D+00
    return

  else if ( 1.0D+00 - x == eps .and. 0.0D+00 < c - a - b ) then

    call gamma ( c, gc )
    call gamma ( c - a - b, gcab )
    call gamma ( c - a, gca )
    call gamma ( c - b, gcb )
    hf = gc * gcab /( gca *gcb )
    return

  else if ( 1.0D+00 + x <= eps .and. abs ( c - a + b - 1.0D+00 ) <= eps ) then

    g0 = sqrt ( pi ) * 2.0D+00**( - a )
    call gamma ( c, g1 )
    call gamma ( 1.0D+00 + a / 2.0D+00 - b, g2 )
    call gamma ( 0.5D+00 + 0.5D+00 * a, g3 )
    hf = g0 * g1 / ( g2 * g3 )
    return

  else if ( l2 .or. l3 ) then

    if ( l2 ) then
      nm = int ( abs ( a ) )
    end if

    if ( l3 ) then
      nm = int ( abs ( b ) )
    end if

    hf = 1.0D+00
    r = 1.0D+00

    do k = 1, nm
      r = r * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
        / ( k * ( c + k - 1.0D+00 ) ) * x
      hf = hf + r
    end do

    return

  else if ( l4 .or. l5 ) then

    if ( l4 ) then
      nm = int ( abs ( c - a ) )
    end if

    if ( l5 ) then
      nm = int ( abs ( c - b ) )
    end if

    hf = 1.0D+00
    r  = 1.0D+00
    do k = 1, nm
      r = r * ( c - a + k - 1.0D+00 ) * ( c - b + k - 1.0D+00 ) &
        / ( k * ( c + k - 1.0D+00 ) ) * x
      hf = hf + r
    end do
    hf = ( 1.0D+00 - x )**( c - a - b ) * hf
    return

  end if

  aa = a
  bb = b
  x1 = x
!
!  WARNING: ALTERATION OF INPUT ARGUMENTS A AND B, WHICH MIGHT BE CONSTANTS.
!
  if ( x < 0.0D+00 ) then
    x = x / ( x - 1.0D+00 )
    if ( a < c .and. b < a .and. 0.0D+00 < b ) then
      a = bb
      b = aa
    end if
    b = c - b
  end if

  if ( 0.75D+00 <= x ) then

    gm = 0.0D+00

    if ( abs ( c - a - b - aint ( c - a - b ) ) < 1.0D-15 ) then

      m = int ( c - a - b )
      call gamma ( a, ga )
      call gamma ( b, gb )
      call gamma ( c, gc )
      call gamma ( a + m, gam )
      call gamma ( b + m, gbm )
      call psi ( a, pa )
      call psi ( b, pb )

      if ( m /= 0 ) then
        gm = 1.0D+00
      end if

      do j = 1, abs ( m ) - 1
        gm = gm * j
      end do

      rm = 1.0D+00
      do j = 1, abs ( m )
        rm = rm * j
      end do

      f0 = 1.0D+00
      r0 = 1.0D+00
      r1 = 1.0D+00
      sp0 = 0.0D+00
      sp = 0.0D+00

      if ( 0 <= m ) then

        c0 = gm * gc / ( gam * gbm )
        c1 = - gc * ( x - 1.0D+00 )**m / ( ga * gb * rm )

        do k = 1, m - 1
          r0 = r0 * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
            / ( k * ( k - m ) ) * ( 1.0D+00 - x )
          f0 = f0 + r0
        end do

        do k = 1, m
          sp0 = sp0 + 1.0D+00 / ( a + k - 1.0D+00 ) &
            + 1.0D+00 / ( b + k - 1.0D+00 ) - 1.0D+00 / real ( k, kind = 8 )
        end do

        f1 = pa + pb + sp0 + 2.0D+00 * el + log ( 1.0D+00 - x )
        hw = f1

        do k = 1, 250

          sp = sp + ( 1.0D+00 - a ) / ( k * ( a + k - 1.0D+00 ) ) &
            + ( 1.0D+00 - b ) / ( k * ( b + k - 1.0D+00 ) )

          sm = 0.0D+00
          do j = 1, m
            sm = sm + ( 1.0D+00 - a ) &
              / ( ( j + k ) * ( a + j + k - 1.0D+00 ) ) &
              + 1.0D+00 / ( b + j + k - 1.0D+00 )
          end do

          rp = pa + pb + 2.0D+00 * el + sp + sm + log ( 1.0D+00 - x )

          r1 = r1 * ( a + m + k - 1.0D+00 ) * ( b + m + k - 1.0D+00 ) &
            / ( k * ( m + k ) ) * ( 1.0D+00 - x )

          f1 = f1 + r1 * rp

          if ( abs ( f1 - hw ) < abs ( f1 ) * eps ) then
            exit
          end if

          hw = f1

        end do

        hf = f0 * c0 + f1 * c1

      else if ( m < 0 ) then

        m = - m
        c0 = gm * gc / ( ga * gb * ( 1.0D+00 - x )**m )
        c1 = - ( - 1 )**m * gc / ( gam * gbm * rm )

        do k = 1, m - 1
          r0 = r0 * ( a - m + k - 1.0D+00 ) * ( b - m + k - 1.0D+00 ) &
            / ( k * ( k - m ) ) * ( 1.0D+00 - x )
          f0 = f0 + r0
        end do

        do k = 1, m
          sp0 = sp0 + 1.0D+00 / real ( k, kind = 8 )
        end do

        f1 = pa + pb - sp0 + 2.0D+00 * el + log ( 1.0D+00 - x )

        do k = 1, 250

          sp = sp + ( 1.0D+00 - a ) &
            / ( k * ( a + k - 1.0D+00 ) ) &
            + ( 1.0D+00 - b ) / ( k * ( b + k - 1.0D+00 ) )

          sm = 0.0D+00
          do j = 1, m
            sm = sm + 1.0D+00 / real ( j + k, kind = 8 )
          end do

          rp = pa + pb + 2.0D+00 * el + sp - sm + log ( 1.0D+00 - x )

          r1 = r1 * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
            / ( k * ( m + k ) ) * ( 1.0D+00 - x )

          f1 = f1 + r1 * rp

          if ( abs ( f1 - hw ) < abs ( f1 ) * eps ) then
            exit
          end if

          hw = f1

        end do

        hf = f0 * c0 + f1 * c1

      end if

    else

      call gamma ( a, ga )
      call gamma ( b, gb )
      call gamma ( c, gc )
      call gamma ( c - a, gca )
      call gamma ( c - b, gcb )
      call gamma ( c - a - b, gcab )
      call gamma ( a + b - c, gabc )
      c0 = gc * gcab / ( gca * gcb )
      c1 = gc * gabc / ( ga * gb ) * ( 1.0D+00 - x )**( c - a - b )
      hf = 0.0D+00
      r0 = c0
      r1 = c1

      do k = 1, 250

        r0 = r0 * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
          / ( k * ( a + b - c + k ) ) * ( 1.0D+00 - x )

        r1 = r1 * ( c - a + k - 1.0D+00 ) * ( c - b + k - 1.0D+00 ) &
          / ( k * ( c - a - b + k ) ) * ( 1.0D+00 - x )

        hf = hf + r0 + r1

        if ( abs ( hf - hw ) < abs ( hf ) * eps ) then
          exit
        end if

        hw = hf

      end do

      hf = hf + c0 + c1

    end if

  else

    a0 = 1.0D+00

    if ( a < c .and. c < 2.0D+00 * a .and. b < c .and. c < 2.0D+00 * b ) then

      a0 = ( 1.0D+00 - x )**( c - a - b )
      a = c - a
      b = c - b

    end if

    hf = 1.0D+00
    r = 1.0D+00

    do k = 1, 250

      r = r * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
        / ( k * ( c + k - 1.0D+00 ) ) * x

      hf = hf + r

      if ( abs ( hf - hw ) <= abs ( hf ) * eps ) then
        exit
      end if

      hw = hf

    end do

    hf = a0 * hf

  end if

  if ( x1 < 0.0D+00 ) then
    x = x1
    c0 = 1.0D+00 / ( 1.0D+00 - x )**aa
    hf = c0 * hf
  end if

  a = aa
  b = bb

  if ( 120 < k ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'HYGFX - Warning!'
    write ( *, '(a)' ) '  A large number of iterations were needed.'
    write ( *, '(a)' ) '  The accuracy of the results should be checked.'
  end if

  return
end
subroutine hygfz ( a, b, c, z, zhf )

!*****************************************************************************80
!
!! HYGFZ computes the hypergeometric function F(a,b,c,x) for complex argument.
!
!  Licensing:
!
!    This routine is copyrighted by Shanjie Zhang and Jianming Jin.  However, 
!    they give permission to incorporate this routine into a user program 
!    provided that the copyright is acknowledged.
!
!  Modified:
!
!    03 August 2012
!
!  Author:
!
!    Shanjie Zhang, Jianming Jin
!
!  Reference:
!
!    Shanjie Zhang, Jianming Jin,
!    Computation of Special Functions,
!    Wiley, 1996,
!    ISBN: 0-471-11963-6,
!    LC: QA351.C45.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) A, B, C, parameters.
!
!    Input, complex ( kind = 8 ) Z, the argument.
!
!    Output, complex ( kind = 8 ) ZHF, the value of F(a,b,c,z).
!
  implicit none

  real ( kind = 8 ) a
  real ( kind = 8 ) a0
  real ( kind = 8 ) aa
  real ( kind = 8 ) b
  real ( kind = 8 ) bb
  real ( kind = 8 ) c
  real ( kind = 8 ) ca
  real ( kind = 8 ) cb
  real ( kind = 8 ) el
  real ( kind = 8 ) eps
  real ( kind = 8 ) g0
  real ( kind = 8 ) g1
  real ( kind = 8 ) g2
  real ( kind = 8 ) g3
  real ( kind = 8 ) ga
  real ( kind = 8 ) gab
  real ( kind = 8 ) gabc
  real ( kind = 8 ) gam
  real ( kind = 8 ) gb
  real ( kind = 8 ) gba
  real ( kind = 8 ) gbm
  real ( kind = 8 ) gc
  real ( kind = 8 ) gca
  real ( kind = 8 ) gcab
  real ( kind = 8 ) gcb
  real ( kind = 8 ) gcbk
  real ( kind = 8 ) gm
  integer ( kind = 4 ) j
  integer ( kind = 4 ) k
  logical l0
  logical l1
  logical l2
  logical l3
  logical l4
  logical l5
  logical l6
  integer ( kind = 4 ) m
  integer ( kind = 4 ) mab
  integer ( kind = 4 ) mcab
  integer ( kind = 4 ) nca
  integer ( kind = 4 ) ncb
  integer ( kind = 4 ) nm
  real ( kind = 8 ) pa
  real ( kind = 8 ) pac
  real ( kind = 8 ) pb
  real ( kind = 8 ) pca
  real ( kind = 8 ) pi
  real ( kind = 8 ) rk1
  real ( kind = 8 ) rk2
  real ( kind = 8 ) rm
  real ( kind = 8 ) sj1
  real ( kind = 8 ) sj2
  real ( kind = 8 ) sm
  real ( kind = 8 ) sp
  real ( kind = 8 ) sp0
  real ( kind = 8 ) sq
  real ( kind = 8 ) t0
  real ( kind = 8 ) w0
  real ( kind = 8 ) ws
  real ( kind = 8 ) x
  real ( kind = 8 ) y
  complex ( kind = 8 ) z
  complex ( kind = 8 ) z00
  complex ( kind = 8 ) z1
  complex ( kind = 8 ) zc0
  complex ( kind = 8 ) zc1
  complex ( kind = 8 ) zf0
  complex ( kind = 8 ) zf1
  complex ( kind = 8 ) zhf
  complex ( kind = 8 ) zp
  complex ( kind = 8 ) zp0
  complex ( kind = 8 ) zr
  complex ( kind = 8 ) zr0
  complex ( kind = 8 ) zr1
  complex ( kind = 8 ) zw

  x = real ( z, kind = 8 )
  y = imag ( z )
  eps = 1.0D-15
  l0 = c == int ( c ) .and. c < 0.0D+00
  l1 = abs ( 1.0D+00 - x ) < eps .and. y == 0.0D+00 .and. &
    c - a - b <= 0.0D+00
  l2 = abs ( z + 1.0D+00 ) < eps .and. &
    abs ( c - a + b - 1.0D+00 ) < eps
  l3 = a == int ( a ) .and. a < 0.0D+00
  l4 = b == int ( b ) .and. b < 0.0D+00
  l5 = c - a == int ( c - a ) .and. c - a <= 0.0D+00
  l6 = c - b == int ( c - b ) .and. c - b <= 0.0D+00
  aa = a
  bb = b
  a0 = abs ( z )
  if ( 0.95D+00 < a0 ) then
    eps = 1.0D-08
  end if
  pi = 3.141592653589793D+00
  el = 0.5772156649015329D+00

  if ( l0 .or. l1 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'HYGFZ - Fatal error!'
    write ( *, '(a)' ) '  The hypergeometric series is divergent.'
    stop
  end if

  if ( a0 == 0.0D+00 .or. a == 0.0D+00 .or. b == 0.0D+00 ) then

    zhf = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )

  else if ( z == 1.0D+00.and. 0.0D+00 < c - a - b ) then

    call gamma ( c, gc )
    call gamma ( c - a - b, gcab )
    call gamma ( c - a, gca )
    call gamma ( c - b, gcb )
    zhf = gc * gcab / ( gca * gcb )

  else if ( l2 ) then

    g0 = sqrt ( pi ) * 2.0D+00 ** ( - a )
    call gamma ( c, g1 )
    call gamma ( 1.0D+00 + a / 2.0D+00 - b, g2 )
    call gamma ( 0.5D+00 + 0.5D+00 * a, g3 )
    zhf = g0 * g1 / ( g2 * g3 )

  else if ( l3 .or. l4 ) then

    if ( l3 ) then
      nm = int ( abs ( a ) )
    end if

    if ( l4 ) then
      nm = int ( abs ( b ) )
    end if

    zhf = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
    zr = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
    do k = 1, nm
      zr = zr * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
        / ( k * ( c + k - 1.0D+00 ) ) * z
      zhf = zhf + zr
    end do

  else if ( l5 .or. l6 ) then

    if ( l5 ) then
      nm = int ( abs ( c - a ) )
    end if

    if ( l6 ) then
      nm = int ( abs ( c - b ) )
    end if

    zhf = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
    zr = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
    do k = 1, nm
      zr = zr * ( c - a + k - 1.0D+00 ) * ( c - b + k - 1.0D+00 ) &
        / ( k * ( c + k - 1.0D+00 ) ) * z
      zhf = zhf + zr
    end do
    zhf = ( 1.0D+00 - z ) ** ( c - a - b ) * zhf

  else if ( a0 <= 1.0D+00 ) then

    if ( x < 0.0D+00 ) then

      z1 = z / ( z - 1.0D+00 )
      if ( a < c .and. b < a .and. 0.0D+00 < b ) then  
        a = bb
        b = aa
      end if
      zc0 = 1.0D+00 / ( ( 1.0D+00 - z ) ** a )
      zhf = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
      zr0 = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
      do k = 1, 500
        zr0 = zr0 * ( a + k - 1.0D+00 ) * ( c - b + k - 1.0D+00 ) &
          / ( k * ( c + k - 1.0D+00 ) ) * z1
        zhf = zhf + zr0
        if ( abs ( zhf - zw ) < abs ( zhf ) * eps ) then
          exit
        end if
        zw = zhf
      end do

      zhf = zc0 * zhf

    else if ( 0.90D+00 <= a0 ) then

      gm = 0.0D+00
      mcab = int ( c - a - b + eps * sign ( 1.0D+00, c - a - b ) )

      if ( abs ( c - a - b - mcab ) < eps ) then

        m = int ( c - a - b )
        call gamma ( a, ga )
        call gamma ( b, gb )
        call gamma ( c, gc )
        call gamma ( a + m, gam )
        call gamma ( b + m, gbm ) 
        call psi ( a, pa )
        call psi ( b, pb )
        if ( m /= 0 ) then
          gm = 1.0D+00
        end if
        do j = 1, abs ( m ) - 1
          gm = gm * j
        end do
        rm = 1.0D+00
        do j = 1, abs ( m )
          rm = rm * j
        end do
        zf0 = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
        zr0 = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
        zr1 = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
        sp0 = 0.0D+00
        sp = 0.0D+00

        if ( 0 <= m ) then

          zc0 = gm * gc / ( gam * gbm )
          zc1 = - gc * ( z - 1.0D+00 ) ** m / ( ga * gb * rm )
          do k = 1, m - 1
            zr0 = zr0 * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
              / ( k * ( k - m ) ) * ( 1.0D+00 - z )
            zf0 = zf0 + zr0
          end do
          do k = 1, m
            sp0 = sp0 + 1.0D+00 / ( a + k - 1.0D+00 ) &
              + 1.0D+00 / ( b + k - 1.0D+00 ) - 1.0D+00 / k
          end do
          zf1 = pa + pb + sp0 + 2.0D+00 * el + log ( 1.0D+00 - z )
          do k = 1, 500
            sp = sp + ( 1.0D+00 - a ) &
              / ( k * ( a + k - 1.0D+00 ) ) + ( 1.0D+00 - b ) &
              / ( k * ( b + k - 1.0D+00 ) )
            sm = 0.0D+00
            do j = 1, m
              sm = sm + ( 1.0D+00 - a ) / ( ( j + k ) &
                * ( a + j + k - 1.0D+00 ) ) &
                + 1.0D+00 / ( b + j + k - 1.0D+00 )
            end do
            zp = pa + pb + 2.0D+00 * el + sp + sm  + log ( 1.0D+00 - z )
            zr1 = zr1 * ( a + m + k - 1.0D+00 ) &
              * ( b + m + k - 1.0D+00 ) / ( k * ( m + k ) ) &
              * ( 1.0D+00 - z )
            zf1 = zf1 + zr1 * zp
            if ( abs ( zf1 - zw ) < abs ( zf1 ) * eps ) then
              exit
            end if
            zw = zf1
          end do

          zhf = zf0 * zc0 + zf1 * zc1

        else if ( m < 0 ) then

          m = - m
          zc0 = gm * gc / ( ga * gb * ( 1.0D+00 - z ) ** m )
          zc1 = - ( - 1.0D+00 ) ** m * gc / ( gam * gbm * rm )
          do k = 1, m - 1
            zr0 = zr0 * ( a - m + k - 1.0D+00 ) &
              * ( b - m + k - 1.0D+00 ) / ( k * ( k - m ) ) &
              * ( 1.0D+00 - z )
            zf0 = zf0 + zr0
          end do

          do k = 1, m
            sp0 = sp0 + 1.0D+00 / k
          end do

          zf1 = pa + pb - sp0 + 2.0D+00 * el + log ( 1.0D+00 - z )

          do k = 1, 500
            sp = sp + ( 1.0D+00 - a ) / ( k * ( a + k - 1.0D+00 ) ) &
              + ( 1.0D+00 - b ) / ( k * ( b + k - 1.0D+00 ) )
            sm = 0.0D+00
            do j = 1, m
              sm = sm + 1.0D+00 / ( j + k )
            end do
            zp = pa + pb + 2.0D+00 * el + sp - sm + log ( 1.0D+00 - z )
            zr1 = zr1 * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
              / ( k * ( m + k ) ) * ( 1.0D+00 - z )
            zf1 = zf1 + zr1 * zp
            if ( abs ( zf1 - zw ) < abs ( zf1 ) * eps ) then
              exit
            end if
            zw = zf1

          end do

          zhf = zf0 * zc0 + zf1 * zc1

        end if

      else

        call gamma ( a, ga )
        call gamma ( b, gb )
        call gamma ( c, gc )
        call gamma ( c - a, gca )
        call gamma ( c - b, gcb )
        call gamma ( c - a - b, gcab )
        call gamma ( a + b - c, gabc )
        zc0 = gc * gcab / ( gca * gcb )
        zc1 = gc * gabc / ( ga * gb ) * ( 1.0D+00 - z ) ** ( c - a - b )
        zhf = cmplx ( 0.0D+00, 0.0D+00, kind = 8 )
        zr0 = zc0
        zr1 = zc1
        do k = 1, 500
          zr0 = zr0 * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
            / ( k * ( a + b - c + k ) ) * ( 1.0D+00 - z )
          zr1 = zr1 * ( c - a + k - 1.0D+00 ) &
            * ( c - b + k - 1.0D+00 ) / ( k * ( c - a - b + k ) ) &
            * ( 1.0D+00 - z )
          zhf = zhf + zr0 + zr1
          if ( abs ( zhf - zw ) < abs ( zhf ) * eps ) then
            exit
          end if
          zw = zhf
        end do

        zhf = zhf + zc0 + zc1

      end if

    else

      z00 = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )

      if ( c - a < a .and. c - b < b ) then
        z00 = ( 1.0D+00 - z ) ** ( c - a - b )
        a = c - a
        b = c - b
      end if

      zhf = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )
      zr = cmplx ( 1.0D+00, 0.0D+00, kind = 8 )

      do k = 1, 1500
        zr = zr * ( a + k - 1.0D+00 ) * ( b + k - 1.0D+00 ) &
          / ( k * ( c + k - 1.0D+00 ) ) * z
        zhf = zhf + zr
        if ( abs ( zhf - zw ) <= abs ( zhf ) * eps ) then
          exit
        end if
        zw = zhf
      end do

      zhf = z00 * zhf

    end if

  else if ( 1.0D+00 < a0 ) then

    mab = int ( a - b + eps * sign ( 1.0D+00, a - b ) )

    if ( abs ( a - b - mab ) < eps .and. a0 <= 1.1D+00 ) then
      b = b + eps
    end if

    if ( eps < abs ( a - b - mab ) ) then

      call gamma ( a, ga )
      call gamma ( b, gb )
      call gamma ( c, gc )
      call gamma ( a - b, gab )
      call gamma ( b - a, gba )
      call gamma ( c - a, gca )
      call gamma ( c - b, gcb )
      zc0 = gc * gba / ( gca * gb * ( - z ) ** a )
      zc1 = gc * gab / ( gcb * ga * ( - z ) ** b )
      zr0 = zc0
      zr1 = zc1
      zhf = cmplx ( 0.0D+00, 0.0D+00 )

      do k = 1, 500
        zr0 = zr0 * ( a + k - 1.0D+00 ) * ( a - c + k ) &
          / ( ( a - b + k ) * k * z )
        zr1 = zr1 * ( b + k - 1.0D+00 ) * ( b - c + k ) &
          / ( ( b - a + k ) * k * z )
        zhf = zhf + zr0 + zr1
        if ( abs ( ( zhf - zw ) / zhf ) <= eps ) then
          exit
        end if
        zw = zhf
      end do

      zhf = zhf + zc0 + zc1

    else

      if ( a - b < 0.0D+00 ) then
        a = bb
        b = aa
      end if

      ca = c - a
      cb = c - b
      nca = int ( ca + eps * sign ( 1.0D+00, ca ) )
      ncb = int ( cb + eps * sign ( 1.0D+00, cb ) )

      if ( abs ( ca - nca ) < eps .or. abs ( cb - ncb ) < eps ) then
        c = c + eps
      end if

      call gamma ( a, ga )
      call gamma ( c, gc )
      call gamma ( c - b, gcb )
      call psi ( a, pa )
      call psi ( c - a, pca )
      call psi ( a - c, pac )
      mab = int ( a - b + eps )
      zc0 = gc / ( ga * ( - z ) ** b )
      call gamma ( a - b, gm )
      zf0 = gm / gcb * zc0
      zr = zc0
      do k = 1, mab - 1
        zr = zr * ( b + k - 1.0D+00 ) / ( k * z )
        t0 = a - b - k
        call gamma ( t0, g0 )
        call gamma ( c - b - k, gcbk )
        zf0 = zf0 + zr * g0 / gcbk
      end do

      if ( mab == 0 ) then
        zf0 = cmplx ( 0.0D+00, 0.0D+00, kind = 8 )
      end if

      zc1 = gc / ( ga * gcb * ( - z ) ** a )
      sp = -2.0D+00 * el - pa - pca
      do j = 1, mab
        sp = sp + 1.0D+00 / j
      end do
      zp0 = sp + log ( - z )
      sq = 1.0D+00
      do j = 1, mab
        sq = sq * ( b + j - 1.0D+00 ) * ( b - c + j ) / j
      end do
      zf1 = ( sq * zp0 ) * zc1
      zr = zc1
      rk1 = 1.0D+00
      sj1 = 0.0D+00

      do k = 1, 10000
        zr = zr / z
        rk1 = rk1 * ( b + k - 1.0D+00 ) * ( b - c + k ) / ( k * k )
        rk2 = rk1
        do j = k + 1, k + mab
          rk2 = rk2 * ( b + j - 1.0D+00 ) * ( b - c + j ) / j
        end do
        sj1 = sj1 + ( a - 1.0D+00 ) / ( k * ( a + k - 1.0D+00 ) ) &
          + ( a - c - 1.0D+00 ) / ( k * ( a - c + k - 1.0D+00 ) )
        sj2 = sj1
        do j = k + 1, k + mab
          sj2 = sj2 + 1.0D+00 / j
        end do
        zp = -2.0D+00 * el - pa - pac + sj2 - 1.0D+00 / ( k + a - c ) &
          - pi / tan ( pi * ( k + a - c ) ) + log ( - z ) 
        zf1 = zf1 + rk2 * zr * zp
        ws = abs ( zf1 )
        if ( abs ( ( ws - w0 ) / ws ) < eps ) then
          exit
        end if
        w0 = ws
      end do

      zhf = zf0 + zf1

    end if

  end if

  a = aa
  b = bb
  if ( 150 < k ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'HYGFZ - Warning!'
    write ( *, '(a)' ) '  The solution returned may have low accuracy.'
  end if

  return
end
subroutine psi ( x, ps )

!*****************************************************************************80
!
!! PSI computes the PSI function.
!
!  Licensing:
!
!    The original FORTRAN77 version of this routine is copyrighted by 
!    Shanjie Zhang and Jianming Jin.  However, they give permission to 
!    incorporate this routine into a user program that the copyright 
!    is acknowledged.
!
!  Modified:
!
!    08 September 2007
!
!  Author:
!
!    Original FORTRAN77 by Shanjie Zhang, Jianming Jin.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Shanjie Zhang, Jianming Jin,
!    Computation of Special Functions,
!    Wiley, 1996,
!    ISBN: 0-471-11963-6,
!    LC: QA351.C45
!
!  Parameters:
!
!    Input, real ( kind = 8 ) X, the argument.
!
!    Output, real ( kind = 8 ) PS, the value of the PSI function.
!
  implicit none

  real ( kind = 8 ), parameter :: a1 = -0.83333333333333333D-01
  real ( kind = 8 ), parameter :: a2 =  0.83333333333333333D-02
  real ( kind = 8 ), parameter :: a3 = -0.39682539682539683D-02
  real ( kind = 8 ), parameter :: a4 =  0.41666666666666667D-02
  real ( kind = 8 ), parameter :: a5 = -0.75757575757575758D-02
  real ( kind = 8 ), parameter :: a6 =  0.21092796092796093D-01
  real ( kind = 8 ), parameter :: a7 = -0.83333333333333333D-01
  real ( kind = 8 ), parameter :: a8 =  0.4432598039215686D+00
  real ( kind = 8 ), parameter :: el = 0.5772156649015329D+00
  integer ( kind = 4 ) k
  integer ( kind = 4 ) n
  real ( kind = 8 ), parameter :: pi = 3.141592653589793D+00
  real ( kind = 8 ) ps
  real ( kind = 8 ) s
  real ( kind = 8 ) x
  real ( kind = 8 ) x2
  real ( kind = 8 ) xa

  xa = abs ( x )
  s = 0.0D+00

  if ( x == aint ( x ) .and. x <= 0.0D+00 ) then

    ps = 1.0D+300
    return

  else if ( xa == aint ( xa ) ) then

    n = int ( xa )
    do k = 1, n - 1
      s = s + 1.0D+00 / real ( k, kind = 8 )
    end do

    ps = - el + s

  else if ( xa + 0.5D+00 == aint ( xa + 0.5D+00 ) ) then

    n = int ( xa - 0.5D+00 )

    do k = 1, n
      s = s + 1.0D+00 / real ( 2 * k - 1, kind = 8 )
    end do

    ps = - el + 2.0D+00 * s - 1.386294361119891D+00

  else

    if ( xa < 10.0D+00 ) then

      n = 10 - int ( xa )
      do k = 0, n - 1
        s = s + 1.0D+00 / ( xa + real ( k, kind = 8 ) )
      end do

      xa = xa + real ( n, kind = 8 )

    end if

    x2 = 1.0D+00 / ( xa * xa )

    ps = log ( xa ) - 0.5D+00 / xa + x2 * ((((((( &
             a8   &
      * x2 + a7 ) &
      * x2 + a6 ) &
      * x2 + a5 ) &
      * x2 + a4 ) &
      * x2 + a3 ) &
      * x2 + a2 ) &
      * x2 + a1 )

    ps = ps - s

  end if

  if ( x < 0.0D+00 ) then
    ps = ps - pi * cos ( pi * x ) / sin ( pi * x ) - 1.0D+00 / x
  end if

  return
end

