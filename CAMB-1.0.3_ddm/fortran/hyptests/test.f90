program test
!  use hyp
  implicit none
  real(8) :: pa,pb,pc,pz,im
  real(8) :: at, kappa
  real(8), dimension(100) :: a, re
  integer i
  ! set at and kappa
  at = 1.5
  kappa = 1.5

  pa=1.0
  pb=1.0/kappa
  pc=1.0+1.0/kappa
  
  do i=1,100
    a(i) = exp(log(1.0e-3)+(i*(log(1.0)-log(1.0e-3))/100.0))
    pz=-(a(i)/at)**kappa
    call hyp(pz,pa,pb,pc,re(i),im)
  enddo

  open(1, file = 'data2.dat', status = 'new')
  do i=1,100  
    write(1,*) a(i), re(i)   
  end do  

  close(1)
end program
