program test
  use precision
  implicit none
  real(dl) :: a,b,c,z
  real(dl) :: vl
  a = 2.0_dl
  b = 0.0001_dl
  c = 4.023_dl
  z = -20_dl
  call hyp2f1(1.0_dl,1.0/a,1.0+1.0/a,-(c/b)**a,vl)
  write (*,*) 'hyp2f1 = ', vl
end program test
  


subroutine hyp2f1 (a,b,c,z,hypvl)
      use precision
      implicit none
      real(dl), intent(in) :: a,b,c,z
      real(dl), intent(out) :: hypvl
      character(6), parameter :: stpy='python'
      character(9), parameter :: fname = 'hyp2f1.py'
      character(80) :: cmd
      integer(dl) :: ierror
      character(13) :: ast,bst,cst,zst
!  open (unit=8,file='d.dat',status='scratch',iostat=ierror)
  !  read (8,*) x, y
      write(ast,'(es13.6)') a
      write(bst,'(es13.6)') b
      write(cst,'(es13.6)') c
      write(zst,'(es13.6)') z
      if (ast(1:1) == '*') then
         ast = '-999999.999999'
         write (*,*) 'warning: a too large for hyp2f1,', &
              'set to 999999.999999'
      end if
      if (bst(1:1) == '*') then
         bst = '-999999.999999'
         write (*,*) 'warning: b too large for hyp2f1,', &
         'set to 999999.999999'
      end if
      if (cst(1:1) == '*') then
         cst = '-999999.999999'
         write (*,*) 'warning: c too large for hyp2f1,', &
         'set to 999999.999999'
      end if
      if (zst(1:1) == '*') then
         zst = '-999999.999999'
         write (*,*) 'warning: z too large for hyp2f1,', &
         'set to -999999.999999'
      end if
      cmd = stpy // ' ' // fname // ' ' // ast // ' ' // bst // &
           ' ' // cst // ' ' // zst
      call execute_command_line(cmd)
      open (unit=8,file='hyp2f1.dat',status='old',iostat=ierror)
      read (8,*) hypvl
      close (8)
      write (*,*) zst, z
      write (*,*) cmd
      call execute_command_line('rm hyp2f1.dat')
end subroutine hyp2f1
