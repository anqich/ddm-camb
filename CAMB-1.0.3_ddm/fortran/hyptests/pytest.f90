program test
!  use precision
  implicit none
  character(6), parameter :: stpy='python'
  character(10) :: filename
  character(20) :: msg
  character(60) :: cmd
  integer(4) :: ierror
  real(8) :: x,y,vl
  character(14) :: a,b
!  open (unit=8,file='d.dat',status='old',iostat=ierror)
  !  read (8,*) x, y
  x = 0.5
  y = 4
  write(a,'(f14.5)') x
  write(b,'(f14.5)') y
  filename = 'test.py'
  cmd = stpy // ' ' // filename // ' ' // a // ' ' // b
  call execute_command_line(cmd)
  open (unit=8,file='value.dat',status='old',iostat=ierror)
  read (8,*) vl
  close (unit=8)
  write (*,*) 'sin(a) = ', vl
  write (*,*) cmd
end program
