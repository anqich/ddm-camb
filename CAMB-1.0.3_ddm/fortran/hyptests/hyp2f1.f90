module hypmod
  implicit none
  contains
  subroutine hyp2f1(hgfa,a,zeta,at,kappa)
      use precision
      real(dl), intent(IN) :: a,zeta
      real(dl), intent(IN) :: at,kappa
      real(dl), intent(OUT) :: hgfa
      
