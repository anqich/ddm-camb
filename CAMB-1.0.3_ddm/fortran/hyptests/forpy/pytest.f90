program test
  use precision
  use forpy_mod
  implicit none
  integer :: ierror
  type(tuple) :: args
  type(module_py) :: scipy
  type(object) :: special, res
  real(dl) :: a, b, c, z, vl
  a = 1_dl
  b = 0.5_dl
  c = 1.5_dl
  z = 2.5_dl
  ierror = forpy_initialize()
  ierror = tuple_create(args,4)
  ierror = args%setitem(0,a)
  ierror = args%setitem(1,b)
  ierror = args%setitem(2,c)
  ierror = args%setitem(3,z)
  ierror = import_py(scipy,"scipy")
  ierror = scipy%getattribute(special,"special")
  ierror = call_py(res,special,"hyp2f1",args)
  ierror = cast(vl,res)
  WRITE (*,*) dl
  write (*,*) vl
end program test
