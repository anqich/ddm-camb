from scipy.special import hyp2f1
import numpy as np
import sys

def calc(a,b,c,z):
    f = open('hyp2f1.dat','w')
    f.write(str(hyp2f1(a,b,c,z)))
    f.close
    return

args = sys.argv[1:]
for i,arg in enumerate(args):
    args[i] = float(arg)
calc(*args)
