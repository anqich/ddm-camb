function setup(options_block) result(csconfig)
	use cosmosis_modules
	use camb_interface_tools

	implicit none

	integer(c_int) :: status
	integer(cosmosis_block), value :: options_block
	integer(cosmosis_status) :: csconfig
	integer :: mode

	status = camb_initial_setup(options_block, mode)
	if (status .ne. 0) then
		write(*,*) "CAMB setup error.  Quitting."
		write(*,*) trim(global_error_message)
		stop
	endif

	csconfig = mode

end function setup

function execute_all(block, csconfig) result(status)
	use cosmosis_modules
	use camb
	use camb_interface_tools
	
	implicit none
	integer(cosmosis_status) :: status
	integer(cosmosis_block), value :: block
	integer(c_size_t), value :: csconfig
	!type(CAMBparams) :: params
	type(CAMBdata) :: st

	call SetActiveState(st)
	status = 0

	if (block==0) then
		write(*,*) "Could not get block in CAMB. Error", status
		status=1
		return
	endif

	!write (*,*) "setting params"
	status = camb_interface_set_params(block, st%CP, CAMB_MODE_ALL)
	status = status + camb_interface_setup_zrange(st%CP)
	
	if (status .ne. 0) then
		write(*,*) "Failed to get parameters in CAMB. Error", status
		status=3
		return
	endif
	!write (*,*) "getting results"
	!Run CAMB to compute the c_ell.
	call CAMB_GetResults(st,st%CP, status)
	if (status .ne. 0) then
		write(*,*) "Failed to run camb_getresults.  Error", status
		write(*,*) "Message:", trim(global_error_message)
		return
	endif
	
	status = camb_interface_save_cls(block)
	if (status .ne. 0) then
		write(*,*) "Failed to write cmb cls section. Error", status
		return
	endif

	status = camb_interface_save_transfer(block)
	if (status .ne. 0) then
		write(*,*) "Failed to write transfer section. Error", status
		return
	endif
	
	status = camb_interface_save_sigma8(block)
	if (status .ne. 0) then
		write(*,*) "Failed to save sigma8 parameter. Error", status
		return
	endif
	
	
	status = camb_interface_save_da(block)
	if (status .ne. 0) then
		write(*,*) "Failed to write angular diameter distance data. Error", status
		return
	endif

	! This will only actually do anything if save_growth=T
	! update from Jessie Muir
	status = camb_interface_save_growth_rate(block)
	if (status .ne. 0) then
		write(*,*) "Failed to write transfer section. Error", status
		return
	endif
	
	return


end function execute_all



function execute_cmb(block, csconfig) result(status)
	use cosmosis_modules
	use camb
	use camb_interface_tools
	implicit none
	integer(cosmosis_status) :: status
	integer(cosmosis_block), value :: block
	integer(c_size_t), value :: csconfig
	type(CAMBparams),pointer :: params
	type(CAMBdata) :: st

	call SetActiveState(st)

	!params => state%CP
	status = 0
	
	status = camb_interface_set_params(block, params, CAMB_MODE_CMB)
	if (status .ne. 0) then
		write(*,*) "Failed to get parameters in CAMB"
		status=3
		return
	endif
	
	status = status + camb_interface_setup_zrange(params)
	if (status .ne. 0) then
		write(*,*) "Failed to set redshift ranges in CAMB"
		status=4
		return
	endif


	!Run CAMB to compute the c_ell.  Also get the ells.
	call CAMB_GetResults(st,params, status)
	if (status .ne. 0) then
		write(*,*) "Failed to run camb_getresults."
		return
	endif


	status = camb_interface_save_cls(block)
	if (status .ne. 0) then
		write(*,*) "Failed to save camb cls."
		return
	endif
	
	status = camb_interface_save_da(block, .false.)
	if (status .ne. 0) then
		write(*,*) "Failed to save camb distances."
		return
	endif


	!There is probably some CAMB clean up to be done here, but not sure what.
	return


end function execute_cmb




function execute_bg(block, csconfig) result(status)
	use cosmosis_modules
	use camb
	use camb_interface_tools
	
	implicit none
	integer(cosmosis_status) :: status
	integer(cosmosis_block), value :: block
	integer(c_size_t), value :: csconfig
	type(CAMBparams) :: params
	type(CAMBdata) :: st

	call SetActiveState(st)

	status = 0

	status = camb_interface_set_params(block, params, CAMB_MODE_BG)
	if (status .ne. 0) then
		write(*,*) "Failed to get parameters in CAMB. Error", status
		status=3
		return
	endif

	status = status + camb_interface_setup_zrange(params)
	

	if (status .ne. 0) then
		write(*,*) "Failed to get parameters in CAMB. Error", status
		status=3
		return
	endif


	params%WantCls = .false.
	params%WantScalars = .false.
	params%WantTensors = .false.
	params%WantVectors = .false.
	call st%SetParams(params, status, DoReion=.false.)
	
		if (status .ne. 0) then
		write(*,*) "Failed to set camb params in camb_bg. Error", status
		write(*,*) trim(global_error_message)
		return
	endif

	call SetActiveState(st)
	status = camb_interface_save_da(block, save_density=.false., save_thermal=.false.)
	if (status .ne. 0) then
		write(*,*) "Failed to write angular diameter distance data in fits section. Error", status
		return
	endif
	
		!There is probably some clean up to be done here, but not sure what.
	return


end function execute_bg



function execute_thermal(block, csconfig) result(status)
	use cosmosis_modules
	use camb
	use camb_interface_tools
	
	implicit none
	integer(cosmosis_status) :: status
	integer(cosmosis_block), value :: block
	integer(c_size_t), value :: csconfig
	type(CAMBparams) :: params
	type(CAMBdata) :: st
	
	call SetActiveState(st)

	status = 0
	
	status = camb_interface_set_params(block, params, CAMB_MODE_THERMAL)
	status = status + camb_interface_setup_zrange(params)

	if (status .ne. 0) then
		write(*,*) "Failed to get parameters in CAMB. Error", status
		status=3
		return
	endif


	params%WantCls = .false.
	params%WantScalars = .false.
	params%WantTensors = .false.
	params%WantVectors = .false.
	call st%SetParams(params, status, DoReion=.false.)
	
		if (status .ne. 0) then
		write(*,*) "Failed to set camb params in camb_bg. Error", status
		write(*,*) trim(global_error_message)
		return
	endif

	!Run CAMB thermal stuff to compute the thermal history
	call CAMB_GetResults(st,params, status)
	if (status .ne. 0) then
		write(*,*) "Failed to run camb_getresults.  Error", status
		write(*,*) "Message:", trim(global_error_message)
		return
	endif
	
	status = camb_interface_save_da(block, save_density=.false.)
	if (status .ne. 0) then
		write(*,*) "Failed to write angular diameter distance data in fits section. Error", status
		return
	endif
	
	!There is probably some clean up to be done here, but not sure what.
	return


end function execute_thermal

function execute(block,csconfig) result(status)
	use cosmosis_modules
	use camb_interface_tools
	implicit none

	interface
		function execute_bg(block,csconfig) result(status)
			use cosmosis_modules
			integer(cosmosis_status) :: status
			integer(cosmosis_block), value :: block
			integer(c_size_t), value :: csconfig
		end function execute_bg

		function execute_cmb(block,csconfig) result(status)
			use cosmosis_modules
			integer(cosmosis_status) :: status
			integer(cosmosis_block), value :: block
			integer(c_size_t), value :: csconfig
		end function execute_cmb

		function execute_thermal(block,csconfig) result(status)
			use cosmosis_modules
			integer(cosmosis_status) :: status
			integer(cosmosis_block), value :: block
			integer(c_size_t), value :: csconfig
		end function execute_thermal

		function execute_all(block,csconfig) result(status)
			use cosmosis_modules
			integer(cosmosis_status) :: status
			integer(cosmosis_block), value :: block
			integer(c_size_t), value :: csconfig
		end function execute_all
    end interface


	integer(cosmosis_status) :: status
	integer(cosmosis_block), value :: block
	integer(c_size_t), value :: csconfig
	integer :: mode

	mode=csconfig
	if (mode==CAMB_MODE_BG) then
		status = execute_bg(block,csconfig)
	else if (mode==CAMB_MODE_CMB) then
		status = execute_cmb(block,csconfig)
	else if (mode==CAMB_MODE_ALL) then
		status = execute_all(block,csconfig)
	else if (mode==CAMB_MODE_THERMAL) then
		status = execute_thermal(block,csconfig)
	else
		write(*,*) "Unknown camb mode.  Was expecting one of:"
		write(*,*) CAMB_MODE_BG,  " -- background_only"
		write(*,*) CAMB_MODE_CMB, " -- cmb"
		write(*,*) CAMB_MODE_ALL, " -- all"
		write(*,*) CAMB_MODE_THERMAL, " -- thermal (background w/ thermal history)"
		write(*,*) "This should be set in the pipeline ini file."
		status = 1
	endif

	if (associated(state)) then
		call state%Free()
	endif

end function execute


function cleanup(csconfig) result(status)
	use camb
	use cosmosis_modules
	integer(c_size_t), value :: csconfig
	integer(cosmosis_status) :: status

	!CAMB has its own built-in clean-up function
	!if (associated(state)) then
	!	call state%Free()
	!endif

end function cleanup
