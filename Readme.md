This is a Boltzmann code modified from CAMB-1.0.3_ddm for the cosmology where there is conversion from DM to DR (https://arxiv.org/pdf/1803.03644.pdf, https://arxiv.org/abs/2011.04606). 
It works as a module in cosmosis https://bitbucket.org/joezuntz/cosmosis/wiki/Home. Demo files are available under ddm-camb/demo.

## Installation
1. Install cosmosis following the instructions on the wiki page above.
2. git clone https://anqich@bitbucket.org/anqich/ddm-camb.git (recommend under cosmosis/modules folder)
3. source cosmosis/config/setup-cosmosis-xxx (the configuration file on your machine)
4. cd CAMB-1.0.3_ddm/forutils/; make
5. cd ../..; make

After the installation you should be able to run the demo file by typing the following line in the command line:
```
cosmosis demo/params_ddm.ini
```

## Citation
https://journals.aps.org/prd/abstract/10.1103/PhysRevD.103.123528

Thanks to Fiona McCarthy and Colin Hills' code comparisons, the code is at v2.0, which fixes two bugs by Feb 25 2022:

1. Switched the hypergeometric function code for the ddm background evolution calculation to CAMB-1.0.3_ddm/fortran/hyp.f, copyright by Robert C. Forrey. 

2. Correction to the assumption of rho_relativistic ~ a^-4 in the diff_rhopi term. (d rho_dr)/dt term, which is not propotional to a^-4 is taken into account.

