from scipy.special import hyp2f1
import numpy as np
from __future__ import print_function
from cosmosis.datablock import option_section, names

def setup(options):
    verbose = options.get_bool(option_section, "verbose", default=False)
    relations_file = options.get_string(
        option_section, "relations_file", default="")
    allddm = options.get_bool(
        option_section, "allddm", default=False)
    cons = consistency_ddm.cosmology_consistency(verbose, relations_file)
    return cons,allddm

def execute(block, config):


def dmfct(arr,zeta,at,kappa):
    dmfct = 1+zeta*(1-arr**kappa)/(1+arr**kappa/at**kappa)
    return dmfct
