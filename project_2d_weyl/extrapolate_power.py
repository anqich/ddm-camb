"""
This module takes linear and non-linear P(k) and extrapolates
them linearly in log-space out to a specified high k_max

"""
from builtins import range
from cosmosis.datablock import option_section, names
import numpy as np
from numpy import log, exp
from scipy.interpolate import RectBivariateSpline


def linear_extend(x, y, xmin, xmax, nmin, nmax, nfit):
    if xmin < x.min():
        xf = x[:nfit]
        yf = y[:nfit]
        p = np.polyfit(xf, yf, 1)
        xnew = np.linspace(xmin, x.min(), nmin, endpoint=False)
        ynew = np.polyval(p, xnew)
        x = np.concatenate((xnew, x))
        y = np.concatenate((ynew, y))
    if xmax > x.max():
        xf = x[-nfit:]
        yf = y[-nfit:]
        p = np.polyfit(xf, yf, 1)
        xnew = np.linspace(x.max(), xmax, nmax, endpoint=True)
        # skip the first point as it is just the xmax
        xnew = xnew[1:]
        ynew = np.polyval(p, xnew)
        x = np.concatenate((x, xnew))
        y = np.concatenate((y, ynew))
    return x, y

def rescale_weyl_ratio(block):
    matterpower = names.matter_power_lin
    weylpower = "weyl_curvature_spectrum"
    zm,km,pm = block.get_grid(matterpower, "z","k_h", "p_k")
    zw,kw,pw = block.get_grid(weylpower, "z","k_h", "p_k")
    if (zm != zw).any():
        print("Ratio calculation only applicable to spectra with same redshifts.")
        return
    else:
        c_kms = 299792.4580
        omega_m = block[names.cosmological_parameters, "omega_m"]
        h0 = block[names.cosmological_parameters, "h0"]
        scale_factor = 1.5 * (100.0*100.0)/(c_kms*c_kms) * omega_m *h0**2
        ratio = np.ones_like(pm)
        weylspl = RectBivariateSpline(zw,kw,pw)
        for i,z in enumerate(zm):
            for j,k in enumerate(km):
                ratio[i,j] = weylspl(z,k)/scale_factor**2/(1+z)**2/pm[i,j]
        ratio_tr = np.where(ratio>1e3,np.ones_like(ratio),ratio)
        truncated = len(zm)*len(km)-np.count_nonzero(ratio_tr==ratio)
        #print(str(truncated)+" outliers >1e3 were truncated to 1")
    block.put_grid("rescaled_weyl_ratio_lin", "z", zm, "k_h", km, "R", ratio_tr)
    


def extrapolate_section(block, section, kmin, kmax, nmin, nmax, npoint):
    # load current values
    k = block[section, "k_h"]
    z = block[section, "z"]
    nk = len(k)
    nz = len(z)
    # load other current values
    k, z, P = block.get_grid(section, "k_h", "z", "p_k")
    # extrapolate
    P_out = []
    for i in range(nz):
        Pi = P[:, i]
        logk, logp = linear_extend(log(k), log(Pi), log(
            kmin), log(kmax), nmin, nmax, npoint)
        P_out.append(exp(logp))

    k = exp(logk)
    P_out = np.dstack(P_out).squeeze()

    block.replace_grid(section, "z", z, "k_h", k, "P_k", P_out.T)


def setup(options):
    kmax = options.get_double(option_section, "kmax")
    kmin = options.get_double(option_section, "kmin", default=1e10)
    nmin = options.get_int(option_section, "nmin", default=50)
    npoint = options.get_int(option_section, "npoint", default=3)
    nmax = options.get_int(option_section, "nmax", default=200)
    spectrum = options.get_string(option_section,"spectrum",default = "weyl_curvature_spectrum")
    weyl_ratio = options.get_bool(option_section, "weyl_ratio",default = False)
    return {"kmax": kmax, "kmin": kmin, "nmin": nmin, "nmax": nmax, "npoint": npoint,"spectrum":spectrum,"weyl_ratio":weyl_ratio}


def execute(block, config):
    kmin = config['kmin']
    kmax = config['kmax']
    nmin = config['nmin']
    nmax = config['nmax']
    npoint = config['npoint']
    spec = config['spectrum']
    weyl_ratio = config['weyl_ratio']

    # extrapolate non-linear power
    for section in [names.matter_power_nl, names.matter_power_lin,spec]:
        if block.has_section(section):
            extrapolate_section(block, section, kmin, kmax, nmin, nmax, npoint)
    if weyl_ratio:
        rescale_weyl_ratio(block)
            
    
    return 0
