from cosmosis.datablock import option_section, names
def setup(options):
    return 0
def execute(block, config):
    z_w,k_w,p_k_w = (block.get_grid("weyl_curvature_spectrum", "z","k_h", "P_k"))
    c_kms = 299792.4580
    omega_m = block[names.cosmological_parameters, "omega_m"]
    h0 = block[names.cosmological_parameters, "h0"]
    scale_factor = 1.5 * (100.0*100.0)/(c_kms*c_kms) * omega_m*h0**2
    for zi,z in enumerate(z_w):
        p_k_w[zi] = p_k_w[zi]/scale_factor**2/(1+z)**2

    
    block.replace_grid("matter_power_lin","z",z_w,"k_h",k_w,"P_k",p_k_w)
    
    return 0

def cleanup(config):
    return 0
