import time
from cosmosis.datablock import names, option_section
import numpy as np

def setup(options):

    return

def execute(block, config):
    


    block["matter_power_nl","p_k"] = pk
    block["matter_power_nl","k_h"] = k
    block["matter_power_nl","z"] = z
    block["matter_power_nl","_cosmosis_order_p_k"] = u'z_cosmosis_order_k_h'
    return 0

def cleanup(config):
    pass

