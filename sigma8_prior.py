from __future__ import print_function
from cosmosis.datablock import option_section, names
import numpy as np

def setup(options):
    verbose = options.get_bool(option_section, "verbose", default=False)
    sig8min = options.get_double(option_section,"sigma8min", default = 0.4)
    sig8max = options.get_double(option_section,"sigma8max", default = 1.4)
    
    return verbose, sig8min, sig8max
    
def execute(block,config):
    (verbose,sig8min,sig8max) = config
    sigma8 = block["cosmological_parameters","sigma_8"]
    if verbose:
        print("sigma8 = "+str(sigma8))
    if np.isnan(sigma8):
        print("sigma8 is NaN, sigma8 consistency check failed")
        return 1
    elif sigma8>sig8max:
        print("sigma8 is larger than "+str(sig8max)+", sigma8 consistency check failed")
        return 2
    elif sigma8<sig8min:
        print("sigma8 is smaller than "+str(sig8min)+", sigma8 consistency check failed")
        return 3
    else:
        return 0

def cleanup(config):
    pass
